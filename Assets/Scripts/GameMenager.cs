﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMenager : MonoBehaviour
{
    public static GameMenager Menago;

    public int RedScore = 0;
    public int GreenScore = 0;

    public Rigidbody2D[] PlayerModels;
    public Transform[] SpawnPoints;

    public Transform Camera;
    private Rigidbody2D Roller;
    private GameObject[] Players;

    private void Awake()
    {
        Menago = this;
    }

    private void Start()
    {
        Roller = GameObject.FindGameObjectWithTag("Roller").transform.GetComponent<Rigidbody2D>();
        for(int i=0; i < GameData.Data.NumberOfPlayers; i++)
        {
            Rigidbody2D go = Instantiate(PlayerModels[2*GameData.Data.ChosenModel[i]+i%2], SpawnPoints[i].position, SpawnPoints[i].rotation);
            go.transform.GetComponent<PlayerControler>().PlayerId = i + 1;
        }
        Players = GameObject.FindGameObjectsWithTag("Player");
        Time.timeScale = 1;
    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                GameUI.UI.Pause();
            }
        }
    }

    //menaging points

    public void AddPoint(int LosingTeam)
    {
        if (LosingTeam == 1)
        {
            if (GreenScore >= 6)
            {
                EndGame(1);
            }
            else
            {
                GreenScore += 1;
                GameUI.UI.AddPointGreen();
            }
        }

        if (LosingTeam == 2)
        {
            if (RedScore >= 6)
            {
                EndGame(-1);
            }
            else
            {
                RedScore += 1;
                GameUI.UI.AddPointRed();
            }
        }

        StartCoroutine("BigShake");

        //reseting game
        GameObject[] toDestroy = GameObject.FindGameObjectsWithTag("Object");
        for(int i = 0; i < toDestroy.Length; i++)
        {
            Destroy(toDestroy[i]);
        }
        Roller.transform.position = new Vector3(0, 0, 0);
        Roller.velocity = new Vector2(0, 0);
        for (int i = 0; i < Players.Length; i++)
        {
            Players[i].transform.GetComponent<PlayerControler>().Exsplode();
        }
    }

    private void EndGame(int winningTeam)
    {
        GameUI.UI.EndGame(winningTeam);
        StartCoroutine("End");
        
    }

    IEnumerator End()
    {
        StartCoroutine("BigShake");
        for(int i = 0; i < 22; i++)
        {
            yield return new WaitForFixedUpdate();
        }
        Time.timeScale = 0;
    }


    //shaking camera

    public void ShakeCamera()
    {
        StartCoroutine("Shake");
    }

    IEnumerator Shake()
    {
        for(int i=0;i<5; i++)
        {
            Camera.position = new Vector3(Camera.position.x + 0.025f, Camera.position.y + 0.04f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        for (int i = 0; i < 5; i++)
        {
            Camera.position = new Vector3(Camera.position.x - 0.05f, Camera.position.y - 0.08f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        for (int i = 0; i < 5; i++)
        {
            Camera.position = new Vector3(Camera.position.x + 0.025f, Camera.position.y + 0.04f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        
    }

    public void BigShakeCamera()
    {
        StartCoroutine("BigShake");
    }

    IEnumerator BigShake()
    {
        for (int i = 0; i < 7; i++)
        {
            Camera.position = new Vector3(Camera.position.x + 0.03f, Camera.position.y - 0.07f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        for (int i = 0; i < 7; i++)
        {
            Camera.position = new Vector3(Camera.position.x - 0.06f, Camera.position.y + 0.14f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        for (int i = 0; i < 7; i++)
        {
            Camera.position = new Vector3(Camera.position.x + 0.03f, Camera.position.y - 0.07f, Camera.position.z);
            yield return new WaitForFixedUpdate();
        }
        Camera.position = new Vector3(0, 0, Camera.position.z);
    }
}
