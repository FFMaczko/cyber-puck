﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controls : MonoBehaviour {

    public static Controls ControlSheme;

    private KeyCode[,] controls; 

    private void Awake()
    {
        // using Singelton on ControlSheme
        if (ControlSheme == null)
        {
            ControlSheme = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        // setting default controlSheme
        controls = new KeyCode[4,3];

        controls[0, 0] = KeyCode.A;
        controls[0, 1] = KeyCode.D;
        controls[0, 2] = KeyCode.S;

        controls[1, 0] = KeyCode.LeftArrow;
        controls[1, 1] = KeyCode.RightArrow;
        controls[1, 2] = KeyCode.DownArrow;

        controls[2, 0] = KeyCode.V;
        controls[2, 1] = KeyCode.N;
        controls[2, 2] = KeyCode.B;

        controls[3, 0] = KeyCode.J;
        controls[3, 1] = KeyCode.L;
        controls[3, 2] = KeyCode.K;

    }

    public void SetKey(int Player, int KeyId, KeyCode Key)
    {
        controls[Player-1,KeyId] = Key;
    }

    public KeyCode[] GetControls(int Player)
    {
        KeyCode[] cont = new KeyCode[3];
        for(int i = 0; i < 3; i++)
        {
            cont[i] = controls[Player - 1,i];
        }

        return cont;
    }
}
