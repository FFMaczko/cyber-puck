﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spark : MonoBehaviour {

    public float size;

	// Use this for initialization
	void Start () {
        transform.localScale = new Vector3(size , size, transform.localScale.z);
        transform.GetComponent<Rigidbody2D>().AddForce(transform.up*Random.Range(400,800));
        StartCoroutine("Fade");
	}
	
	IEnumerator Fade()
    {
        while (transform.localScale.x>0)
        {
            yield return new WaitForSeconds(0.01f);
            transform.localScale = new Vector3(transform.localScale.x - 0.02f, transform.localScale.y - 0.02f, transform.localScale.z);
        }
        Destroy(gameObject);
    }
}
