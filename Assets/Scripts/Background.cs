﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public Camera mainCamera;

    private int Red = 75;
    private int Green=25;
    private int Blue = 25;

    private int RedAmp = 1;
    private int GreenAmp = -1;
    private int BlueAmp = 1;


    // Use this for initialization
    void Start () {
        StartCoroutine("Fade");
	}
	
    IEnumerator Fade()
    {
        while (true)
        {
            
            Blue += BlueAmp;
            if (Blue == 255)
            {
                BlueAmp = -1;
            }
            if(Blue == 0)
            {
                BlueAmp = 1;
            }

            Green += GreenAmp;
            if (Green == 50)
            {
                GreenAmp = -1;
            }
            if (Green == 0)
            {
                GreenAmp = 1;
            }

            Red += RedAmp;
            if (Red == 90)
            {
                RedAmp = -1;
            }
            if (Red == 0)
            {
                RedAmp = 1;
            }

            mainCamera.backgroundColor = new Color32((byte)Red, (byte)Green, (byte)Blue, 255);
            yield return new WaitForSecondsRealtime(0.04f);
        }
    }
}
