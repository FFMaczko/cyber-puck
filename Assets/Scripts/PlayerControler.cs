﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControler : MonoBehaviour {

    public int PlayerId;

    private KeyCode[] controls;

    private float RotationAngel = 4f;
    private float RecoilSpeed=8;
    private Rigidbody2D body;

    public Rigidbody2D bullet;
    public Transform Barrel;
    private bool CanShoot;
    private int timeBetweenShots = 400;

    public GameObject Exsplosion;
    private Vector3 StartPosition;
    private Quaternion StartRotation;

    private AudioSource Audio;
    public AudioClip ShootSound;
    public AudioClip ExplosionSound;

    public GameObject Smoke;

    // Use this for initialization
    void Start () {
        controls = Controls.ControlSheme.GetControls(PlayerId);
        body = transform.GetComponent<Rigidbody2D>();
        Audio = transform.GetComponent<AudioSource>();
        SoundMenager.Menager.AddAudioSource(Audio);
        CanShoot = true;
        StartRotation = transform.rotation;
        StartPosition = transform.position;
	}

    private void FixedUpdate () {
        // controls
        if (Input.GetKey(controls[0]))
        {
            Turn(1);
        }

        if (Input.GetKey(controls[1]))
        {
            Turn(-1);
        }

        if (Input.GetKey(controls[2]))
        {
            Shoot();
        }
    }

    //movement
    private void Turn(int i)
    {
        transform.Rotate(new Vector3(0, 0, RotationAngel * Mathf.Sign(i)));
    }

    //shooting
    private void Shoot()
    {
        if (CanShoot)
        {
            body.velocity= transform.up * RecoilSpeed;
            Instantiate(bullet, Barrel.position, Barrel.rotation);
            Audio.PlayOneShot(ShootSound);
            for (int i = 0; i < 15; i++)
            {
                GameObject go = Instantiate(Smoke, Barrel.position, Barrel.rotation);
                go.transform.Rotate(new Vector3(0, 0, Random.Range(-60, 60)));
            }
            StartCoroutine("ShootDelay");
        }
    }

    IEnumerator ShootDelay()
    {
        CanShoot = false;
        yield return new WaitForSeconds(timeBetweenShots * 0.001f);
        CanShoot = true;
    }

    //
    public void Exsplode()
    {
        StartCoroutine("ExplosionEn");
    }

    IEnumerator ExplosionEn()
    {
        Instantiate(Exsplosion, transform.position, transform.rotation);
        Audio.PlayOneShot(ExplosionSound);
        GameMenager.Menago.BigShakeCamera();
        transform.position = new Vector3(-100, -100, 0);
        for (int i = 0; i < 21; i++)
        { 
            yield return new WaitForFixedUpdate();
        }
        transform.position = StartPosition;
        transform.rotation = StartRotation;
        body.velocity = new Vector2(0, 0);
    }
}
