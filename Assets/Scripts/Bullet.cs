﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    private int speed = 10;

    public GameObject Exsplosion;

    void Start () {
        transform.GetComponent<Rigidbody2D>().velocity = transform.up * speed;
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Wall")
        {
            Exsplode();
        }

        if(collision.tag == "Object")
        {
            GameMenager.Menago.ShakeCamera();
            Exsplode();
        }

        if (collision.tag == "Roller")
        {
            collision.transform.GetComponent<Rigidbody2D>().velocity += transform.GetComponent<Rigidbody2D>().velocity/2;
            Exsplode();
        }

        if (collision.tag == "Player")
        {
            collision.GetComponent<PlayerControler>().Exsplode();
            Exsplode();
        }
    }

    private void Exsplode()
    {
        Instantiate(Exsplosion, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
