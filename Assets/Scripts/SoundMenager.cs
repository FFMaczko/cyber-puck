﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMenager : MonoBehaviour {

    private float MasterVolume;
    private float LastVolume;
    private LinkedList<AudioSource> AudioSources;

    public static SoundMenager Menager;

    private void Awake()
    {
        Menager = this;
        AudioSources = new LinkedList<AudioSource>();
        LastVolume = MasterVolume = 1;
    }

    public float GetVolume()
    {
        return MasterVolume;
    }

    public void Mute()
    {
        LastVolume = MasterVolume;
        MasterVolume = 0;
        foreach(AudioSource a in AudioSources)
        {
            a.volume = MasterVolume;
        }
    }

    public void Unmute()
    {
        MasterVolume = LastVolume;
        foreach (AudioSource a in AudioSources)
        {
            a.volume = MasterVolume;
        }
    }

    public void AddAudioSource(AudioSource a)
    {
        AudioSources.AddLast(a);
        a.volume = MasterVolume;
    }

    public void DestroyAudioSource(AudioSource a)
    {
        AudioSources.Remove(a);
    }

}
