﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenuOptions : MonoBehaviour {

    public Sprite[] PlayerModels;

    public Text GameModeText;
    private bool SoloMode;

    public Canvas OneVsOneMenu;
    public Canvas DualMenu;

    public Image[] PlayerImages;

    void Start () {
        SoloMode = true;
        DualMenu.enabled = false;
        OneVsOneMenu.enabled = true;
        if (GameData.Data != null)
        {
            PlayerImages[0].sprite = PlayerModels[2 * GameData.Data.ChosenModel[0]];
            PlayerImages[1].sprite = PlayerModels[2 * GameData.Data.ChosenModel[0]];
            PlayerImages[2].sprite = PlayerModels[2 * GameData.Data.ChosenModel[1] + 1];
            PlayerImages[3].sprite = PlayerModels[2 * GameData.Data.ChosenModel[1] + 1];
            PlayerImages[4].sprite = PlayerModels[2 * GameData.Data.ChosenModel[2]];
            PlayerImages[5].sprite = PlayerModels[2 * GameData.Data.ChosenModel[3]+1];
        }
	}
	
    public void ChangeGameMode()
    {
        if (SoloMode)
        {
            GameModeText.text = "2 vs 2";
            SoloMode = false;
            GameData.Data.NumberOfPlayers = 4;
            OneVsOneMenu.enabled = false;
            DualMenu.enabled = true;
        }
        else
        {
            GameModeText.text = "1 vs 1";
            SoloMode = true;
            GameData.Data.NumberOfPlayers = 2;
            OneVsOneMenu.enabled = true;
            DualMenu.enabled = false;
        }
    }

    public void NextModel(int player)
    {
        if (GameData.Data.ChosenModel[player] == PlayerModels.Length / 2 - 1)
        {
            GameData.Data.ChosenModel[player] = 0;
        }
        else
        {
            GameData.Data.ChosenModel[player] += 1;
        }

        if (player == 0)
        {
            PlayerImages[0].sprite = PlayerModels[2*GameData.Data.ChosenModel[player]];
            PlayerImages[1].sprite = PlayerModels[2*GameData.Data.ChosenModel[player]];
        }
        else if(player == 1)
        {
            PlayerImages[2].sprite = PlayerModels[2*GameData.Data.ChosenModel[player]+1];
            PlayerImages[3].sprite = PlayerModels[2*GameData.Data.ChosenModel[player]+1];
        }
        else
        {
            PlayerImages[player+2].sprite = PlayerModels[2*GameData.Data.ChosenModel[player]+player%2];
        }
    }
}
