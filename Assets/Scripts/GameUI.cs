﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

    public static GameUI UI;

    public Image SoundButton;

    //points
    public Image[] RedLights;
    private int LightedRed;

    public Image[] GreenLights;
    private int LightedGreen;

    public Image FinalPoint;
    public Text Winner;

    //menus
    public Canvas PauseMenu;
    public Canvas EndMenu;


	void Start () {
        UI = this;
        LightedGreen = 0;
        LightedRed = 0;
        PauseMenu.enabled = false;
        EndMenu.enabled = false;
	}


    //ui buttons

    public void Pause()
    {
        if (Time.timeScale == 1) { 
        Time.timeScale = 0;
        PauseMenu.enabled = true;
        }
    }

    public void UnPause()
    {
        Time.timeScale = 1;
        PauseMenu.enabled = false;
    }

    public void Sound()
    {
        if (SoundMenager.Menager.GetVolume() != 0)
        {
            SoundMenager.Menager.Mute();
            SoundButton.color = new Color32(109, 109, 109, 255);
        }
        else
        {
            SoundMenager.Menager.Unmute();
            SoundButton.color = new Color32(255, 0, 236, 255);
        }
    }

    //points lights

    public void AddPointRed()
    {
        RedLights[LightedRed].color = new Color32(255,0,0,255);
        LightedRed += 1;
    }

    public void AddPointGreen()
    {
        GreenLights[LightedGreen].color = new Color32(0, 255, 10, 255);
        LightedGreen += 1;
    }


    // ending game

    public void EndGame(int winner)
    {
        SetFinalPoint(winner);
        SetWinner(winner);
        EndMenu.enabled = true;
    }

    private void SetFinalPoint(int i)
    {
        if (Mathf.Sign(i) < 0)
        {
            FinalPoint.color = new Color32(255, 0, 0, 255);
        }
        if (Mathf.Sign(i) >= 0)
        {
            FinalPoint.color = new Color32(0, 255, 10, 255);
        }
    }

    private void SetWinner(int i)
    {
        if (Mathf.Sign(i) < 0)
        {
            Winner.color = new Color32(255, 0, 0, 255);
            Winner.text = "RED TEAM WINS";
        }
        if (Mathf.Sign(i) >= 0)
        {
            Winner.color = new Color32(0, 255, 10, 255);
            Winner.text = "GREEN TEAM WINS";
        }
    }

}
