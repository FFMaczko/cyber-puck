﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goal : MonoBehaviour {
    public int Team;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Roller")
        {
            GameMenager.Menago.AddPoint(Team);
        }
    }
}
