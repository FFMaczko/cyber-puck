﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exsplosion : MonoBehaviour {

    public GameObject Spark;
    public AudioClip Exp;
    private AudioSource Audio;

    // Use this for initialization
    void Start() {
        Audio = transform.GetComponent<AudioSource>();
        SoundMenager.Menager.AddAudioSource(Audio);
        Audio.PlayOneShot(Exp);
        for (int i = 0; i < 20; i++)
        {
            Instantiate(Spark, transform.position, Quaternion.Euler(new Vector3(0, 0, Random.Range(0, 360))));
        }
        StartCoroutine("DestroyMe");
	}
	
    IEnumerator DestroyMe()
    {
        yield return new WaitForSeconds(0.5f);
        SoundMenager.Menager.DestroyAudioSource(Audio);
        Destroy(gameObject);
    }
}
